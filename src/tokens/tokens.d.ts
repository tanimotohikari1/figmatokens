/**
 * Do not edit directly
 * Generated on Sun, 15 May 2022 14:05:42 GMT
 */

export default tokens;

declare interface DesignToken {
  value: any;
  name?: string;
  comment?: string;
  themeable?: boolean;
  attributes?: {
    category?: string;
    type?: string;
    item?: string;
    subitem?: string;
    state?: string;
    [key: string]: any;
  };
  [key: string]: any;
}

declare const tokens: {
  "colors": {
    "black": DesignToken,
    "white": DesignToken
  },
  "Gray": {
    "10": DesignToken,
    "20": DesignToken,
    "30": DesignToken,
    "40": DesignToken,
    "50": DesignToken,
    "60": DesignToken,
    "70": DesignToken,
    "80": DesignToken,
    "90": DesignToken,
    "100": DesignToken
  },
  "red": {
    "700": DesignToken,
    "800": DesignToken,
    "900": DesignToken
  },
  "Blue": {
    "90": DesignToken,
    "100": DesignToken
  }
}