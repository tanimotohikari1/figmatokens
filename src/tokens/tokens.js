/**
 * Do not edit directly
 * Generated on Sun, 15 May 2022 14:05:42 GMT
 */

module.exports = {
  "colors": {
    "black": {
      "value": "#000000",
      "type": "color",
      "filePath": "output.json",
      "isSource": true,
      "original": {
        "value": "#000000",
        "type": "color"
      },
      "name": "ColorsBlack",
      "attributes": {
        "category": "colors",
        "type": "black"
      },
      "path": [
        "colors",
        "black"
      ]
    },
    "white": {
      "value": "#ffffff",
      "type": "color",
      "filePath": "output.json",
      "isSource": true,
      "original": {
        "value": "#ffffff",
        "type": "color"
      },
      "name": "ColorsWhite",
      "attributes": {
        "category": "colors",
        "type": "white"
      },
      "path": [
        "colors",
        "white"
      ]
    }
  },
  "Gray": {
    "10": {
      "value": "#e9e9e9",
      "type": "color",
      "filePath": "output.json",
      "isSource": true,
      "original": {
        "value": "#e9e9e9",
        "type": "color"
      },
      "name": "Gray10",
      "attributes": {
        "category": "Gray",
        "type": "10"
      },
      "path": [
        "Gray",
        "10"
      ]
    },
    "20": {
      "value": "#d3d3d3",
      "type": "color",
      "filePath": "output.json",
      "isSource": true,
      "original": {
        "value": "#d3d3d3",
        "type": "color"
      },
      "name": "Gray20",
      "attributes": {
        "category": "Gray",
        "type": "20"
      },
      "path": [
        "Gray",
        "20"
      ]
    },
    "30": {
      "value": "#bcbcbc",
      "type": "color",
      "filePath": "output.json",
      "isSource": true,
      "original": {
        "value": "#bcbcbc",
        "type": "color"
      },
      "name": "Gray30",
      "attributes": {
        "category": "Gray",
        "type": "30"
      },
      "path": [
        "Gray",
        "30"
      ]
    },
    "40": {
      "value": "#a6a6a6",
      "type": "color",
      "filePath": "output.json",
      "isSource": true,
      "original": {
        "value": "#a6a6a6",
        "type": "color"
      },
      "name": "Gray40",
      "attributes": {
        "category": "Gray",
        "type": "40"
      },
      "path": [
        "Gray",
        "40"
      ]
    },
    "50": {
      "value": "#909090",
      "type": "color",
      "filePath": "output.json",
      "isSource": true,
      "original": {
        "value": "#909090",
        "type": "color"
      },
      "name": "Gray50",
      "attributes": {
        "category": "Gray",
        "type": "50"
      },
      "path": [
        "Gray",
        "50"
      ]
    },
    "60": {
      "value": "#7a7a7a",
      "type": "color",
      "filePath": "output.json",
      "isSource": true,
      "original": {
        "value": "#7a7a7a",
        "type": "color"
      },
      "name": "Gray60",
      "attributes": {
        "category": "Gray",
        "type": "60"
      },
      "path": [
        "Gray",
        "60"
      ]
    },
    "70": {
      "value": "#646464",
      "type": "color",
      "filePath": "output.json",
      "isSource": true,
      "original": {
        "value": "#646464",
        "type": "color"
      },
      "name": "Gray70",
      "attributes": {
        "category": "Gray",
        "type": "70"
      },
      "path": [
        "Gray",
        "70"
      ]
    },
    "80": {
      "value": "#4d4d4d",
      "type": "color",
      "filePath": "output.json",
      "isSource": true,
      "original": {
        "value": "#4d4d4d",
        "type": "color"
      },
      "name": "Gray80",
      "attributes": {
        "category": "Gray",
        "type": "80"
      },
      "path": [
        "Gray",
        "80"
      ]
    },
    "90": {
      "value": "#373737",
      "type": "color",
      "filePath": "output.json",
      "isSource": true,
      "original": {
        "value": "#373737",
        "type": "color"
      },
      "name": "Gray90",
      "attributes": {
        "category": "Gray",
        "type": "90"
      },
      "path": [
        "Gray",
        "90"
      ]
    },
    "100": {
      "value": "#212121",
      "type": "color",
      "filePath": "output.json",
      "isSource": true,
      "original": {
        "value": "#212121",
        "type": "color"
      },
      "name": "Gray100",
      "attributes": {
        "category": "Gray",
        "type": "100"
      },
      "path": [
        "Gray",
        "100"
      ]
    }
  },
  "red": {
    "700": {
      "value": "#c96c6c",
      "type": "color",
      "filePath": "output.json",
      "isSource": true,
      "original": {
        "value": "#c96c6c",
        "type": "color"
      },
      "name": "Red700",
      "attributes": {
        "category": "red",
        "type": "700"
      },
      "path": [
        "red",
        "700"
      ]
    },
    "800": {
      "value": "#9b2c2c",
      "type": "color",
      "filePath": "output.json",
      "isSource": true,
      "original": {
        "value": "#9b2c2c",
        "type": "color"
      },
      "name": "Red800",
      "attributes": {
        "category": "red",
        "type": "800"
      },
      "path": [
        "red",
        "800"
      ]
    },
    "900": {
      "value": "#500404",
      "type": "color",
      "filePath": "output.json",
      "isSource": true,
      "original": {
        "value": "#500404",
        "type": "color"
      },
      "name": "Red900",
      "attributes": {
        "category": "red",
        "type": "900"
      },
      "path": [
        "red",
        "900"
      ]
    }
  },
  "Blue": {
    "90": {
      "value": "#7eefff",
      "type": "color",
      "filePath": "output.json",
      "isSource": true,
      "original": {
        "value": "#7eefff",
        "type": "color"
      },
      "name": "Blue90",
      "attributes": {
        "category": "Blue",
        "type": "90"
      },
      "path": [
        "Blue",
        "90"
      ]
    },
    "100": {
      "value": "#00a3ff",
      "type": "color",
      "filePath": "output.json",
      "isSource": true,
      "original": {
        "value": "#00a3ff",
        "type": "color"
      },
      "name": "Blue100",
      "attributes": {
        "category": "Blue",
        "type": "100"
      },
      "path": [
        "Blue",
        "100"
      ]
    }
  }
};